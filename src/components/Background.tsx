import React, { Component } from 'react';
import './Background.css'; 
import Image from './Image';
import Login from './Login';

class Background extends Component {
    render() {
        return (
            <div className="container-background">
                <div className="row-background">
                    <Image />
                </div>
                <div className="sm-title-login">Inicia sesión para continuar</div>
                <div className="row-background">
                    <Login/>
                </div>
                <div className="sm-title-registration">
                    ¿Todavia no tienes una cuenta? <a>Registrate</a>
                </div>
            </div>
        );
    }
}

export default Background;
