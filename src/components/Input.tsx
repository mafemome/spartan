interface ContainerProps {
  name: string;
  type: string;
  iconInput: string;
}

const Input: React.FC<ContainerProps> = ({ name, type, iconInput }) => {
  return (
    <div className="input-group">
      <span className="material-icons">{iconInput}</span>
      <input type={type} placeholder={name} />
    </div>
  );
};

export default Input;
