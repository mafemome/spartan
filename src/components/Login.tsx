import React, { Component } from 'react';
import Input from './Input';

class Login extends Component {
    render() {
        return (
            <div className="container-login">
                <h3>Inicia sesión para continuar</h3>
                <Input name={"Correo Electronico"} type={"email"} iconInput="email" />
                <Input name={"Contraseña"} type={"password"} iconInput="lock" />
                <button >INICIAR SESIÓN</button>
                <a href="#" >¿Olvidaste tú contraseña?</a>
                <div>¿Todavia no tienes una cuenta? <a href="#" >Registrate</a></div>
            </div>
        );
    }
}

export default Login;