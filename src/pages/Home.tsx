import { IonContent, IonPage } from '@ionic/react';
import './Home.css';
import Background from '../components/Background';

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonContent fullscreen>
        <Background />
      </IonContent>
    </IonPage>
  );
};

export default Home;
